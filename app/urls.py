from django.conf.urls import *
from app.views import EditView
#from django.contrib import admin
#from django.contrib.admin import site

#admin.autodiscover()

urlpatterns = patterns(
    '',
    url(r'^$', 'app.views.index', name='index'),
    url(r'^signin/$', 'app.views.signin', name='signin'),
    url(r'^signup/$', 'app.views.signup', name='signup'),
    url(r'^signout/$', 'app.views.signout', name='signout'),

    url(r'create/$', EditView.as_view(), name='create'),
    url(r'(?P<edit_id>\d+)/edit/$', EditView.as_view(), name='edit'),


    # enable the admin:
    #url(r'^admin/', include(site.urls)),
)
