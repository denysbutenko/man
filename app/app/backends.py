from app.models import UserProfile


class EmailAuthBackend(object):

    def authenticate(self, username=None, password=None):
        """ Authenticate a user based on email address as the user name. """
        try:
            user = UserProfile.objects.get(email=username)
            if user.check_password(password):
                return user
            else:
                return None
        except UserProfile.DoesNotExist:
            return None

    def authenticate_without_passwd(self, username):
        try:
            user = UserProfile.objects.get(email=username)
            if user:
                return user
        except UserProfile.DoesNotExist:
            return None

    def get_user(self, user_id):
        """ Get a UserProfile object from the user_id. """
        try:
            return UserProfile.objects.get(pk=user_id)
        except UserProfile.DoesNotExist:
            return None
