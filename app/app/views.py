# -*- coding: utf-8 -*-
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render_to_response, get_object_or_404
from django.contrib import messages
from django.shortcuts import redirect
from django.core.urlresolvers import reverse
from django.template import RequestContext
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from .forms import SignInForm, SignUpForm, TicketForm
from .models import UserProfile
from django.views.generic import TemplateView, DetailView


def index(request):
    context = {}
    return render_to_response('index.html', context,
                              context_instance=RequestContext(request))


def signup(request):
    form = SignUpForm()
    register_data = request.session.get('register_data', False)
    if register_data:
        form = SignUpForm(initial=register_data)

    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = UserProfile()
            user.set_password(form.cleaned_data["password"])
            user.email = form.cleaned_data["email"]
            user.name = form.cleaned_data["name"]
            user.is_active = True
            user.save()

            messages.success(request, _(u"Вы успешно создали аккаунт!"))

            return redirect(reverse("signin"))
    context = {
        "form": form,
    }
    return render_to_response('pages/signup.html', context,
                              context_instance=RequestContext(request))


def signin(request):
    context = {}
    email = password = ''

    # Try authenticate user
    if request.method == 'POST':
        email = request.POST.get("email", None)
        password = request.POST.get("password", None)
        user = authenticate(username=email, password=password)

        if not user:
            # User NOT EXISTS
            messages.error(request, _(u"Не удалось войти в систему"))
            return redirect(reverse("signin"))
        else:
            if user.is_active:
                # User SIGNED IN
                login(request, user)

                if request.GET.get('next'):
                    return redirect(request.GET.get('next'))
                else:
                    return redirect(reverse('index'))
            else:
                # User INACTIVE
                messages.error(request, _(u"Ваша учётная запись не активна"))

    # Just show the form
    else:
        form = SignInForm()
        context["form"] = form
        if request.user.is_authenticated():
            if 'next' in request.GET:
                return redirect(request.GET['next'])
            else:
                return redirect(reverse("index"))

    #context["pass_recovery_form"] = PassRecoveryForm()

    return render_to_response('pages/signin.html', context,
                              context_instance=RequestContext(request))


def signout(request):
    logout(request)
    # Redirect to a success page.
    return redirect(reverse("signin"))


class EditView(TemplateView):
    """
        if object.pk exist then edit model
        else create new object
    """
    active_submenu = None
    subtitle = None
    Form = TicketForm
    context = {}
    to_list_url = ''
    template_name = 'forms/tickets.html'

    def __init__(self, *args, **kwargs):
        self.context.update({
#            "to_list_url": reverse(self.to_list_url),
        })
        if not self.Form:
            raise Exception("Class %s should has attribute 'Form'" %
                            self.__class__)
        self.Model = self.Form._meta.model

        return super(EditView, self).__init__(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EditView, self).get_context_data(**kwargs)
        context.update(**self.context)

        edit_id = kwargs.get("edit_id", None)
        context["edit_id"] = edit_id

        if not edit_id:
            """
            Create new
            """
            create_new = True
            form = self.Form()
            context["subtitle"] = "Создание заявки"
        else:
            """
            Edit existed
            """
            create_new = False

            instance = get_object_or_404(self.Model, pk=edit_id)

            context["subtitle"] = "Редактирование заявки"

            form = self.Form(instance=instance)

        # for GET # fix later
        #if "image" in form.fields:
        #    # view_url = reverse('upload_handler')
        #    #upload_url, upload_data = prepare_upload(self.request, "")
        #    context.update({
        #        "upload_url": upload_url,
        #        "upload_data": upload_data,
        #    })

        context.update({
                       "form": form,
                       "create_new": create_new,
                       })

        return context

    def post(self, request, *args, **kwargs):
        response = {}

        edit_id = kwargs.get("edit_id", None)

        if not edit_id:
            form = self.Form(request.POST, request.FILES)
            form.instance.created_by = request.user
        else:
            try:
                instance = self.Model.objects.get(pk=edit_id)
            except self.Model.DoesNotExist:
                raise Http404
            form = self.Form(request.POST, request.FILES, instance=instance)

        if form.is_valid():
            form.instance.created_by = request.user
            form.save()

            if not edit_id:
                response['redirect'] = self.context['to_list_url']

        response.update({
            "saved": form.is_valid(),
            "errors": form.errors,
        })
        return HttpResponse(simplejson.dumps(response, use_decimal=True),
                            mimetype='application/json')
