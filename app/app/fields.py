# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.db import models


class UniqueCodeField(forms.CharField):

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 16
        kwargs['label'] = _(u"Код")
        kwargs['required'] = False
        kwargs['help_text'] = _(u"Код должен быть уникальным и"
                                u" может состоять из цифирок"
                                u" и буковок")
        super(UniqueCodeField, self).__init__(*args, **kwargs)


def clean_unique(form, field, exclude_initial=True,
                 format="%(value)s значение уже существует."):
    value = form.cleaned_data.get(field)
    if value:
        qs = form._meta.model._default_manager.filter(**{field: value})
        if exclude_initial and form.initial:
            initial_value = form.initial.get(field)
            qs = qs.exclude(**{field: initial_value})
        if qs.count() > 0:
            raise forms.ValidationError(
                format % {'field': field, 'value': value})
    return value
