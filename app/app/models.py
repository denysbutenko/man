# -*- coding: utf-8 -*-
from django.db import models
from .fields import UniqueCodeField
from django.contrib.auth.hashers import (check_password, make_password,
                                         is_password_usable)
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin


class BaseModel(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=155)
    created_on = models.DateTimeField(auto_now_add=True)
    edited_on = models.DateTimeField(auto_now=True)
    is_published = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)
    _created_by = models.IntegerField(null=False, blank=False)

    @property
    def created_by(self):
        return get_user_by_id(self._created_by)

    @created_by.setter
    def created_by(self, user):
        if isinstance(user, (int, long)):
            self._created_by = user
        else:
            self._created_by = user.pk

    def __unicode__(self):
        return self.title


class UserProfile(models.Model):
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    email = models.EmailField(help_text=_(u"Email адрес"), unique=True,
                              blank=False, null=False, max_length=254)
    name = models.CharField(max_length=30,
                            help_text=_(u'Имя пользователя'), blank=False)
    password = models.CharField(_('password'), max_length=128, blank=False)
    is_confirmed = models.BooleanField(_('confirmed'), default=True),
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Unselect this instead'
                                                ' of deleting accounts.'))
    last_login = models.DateTimeField(_('last login'), auto_now=True)
    date_joined = models.DateTimeField(_('date joined'), auto_now_add=True)
    org_name = models.CharField(max_length=255)
    phone = models.CharField(max_length=255)

    profile_picture = models.FileField(
        blank=True, null=True, upload_to='uploads/profile_photos/',
    )

    class Meta:
        app_label = 'app'
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __unicode__(self):
        return self.name.strip()

    def natural_key(self):
        return (self.email,)

    def is_authenticated(self):
        """
        Always return True. This is a way to tell if the user has been
        authenticated in templates.
        """
        return True

    def get_full_name(self):
        return self.name.strip()

    def set_password(self, raw_password):
        self.password = make_password(raw_password)

    def check_password(self, raw_password):
        """
        Returns a boolean of whether the raw_password was correct. Handles
        hashing formats behind the scenes.
        """
        def setter(raw_password):
            self.set_password(raw_password)
            self.save()
        return check_password(raw_password, self.password, setter)

    def set_unusable_password(self):
        # Sets a value that will never be a valid hash
        self.password = make_password(None)

    def has_usable_password(self):
        return is_password_usable(self.password)

    def has_module_perms(self, test):
        return False


class Category(BaseModel):
    pass


class Ticket(BaseModel):
    unique_code = models.CharField(max_length=20)
    supervisor = models.ForeignKey(UserProfile)
    image = models.FileField(
        blank=True, null=True, upload_to='uploads/tickets_photos/',)
    city = models.CharField(max_length=255)
    school = models.CharField(max_length=255)
    class_room = models.CharField(max_length=255)
    department = models.CharField(max_length=255)
    section = models.CharField(max_length=255)
    basic = models.CharField(max_length=255)
    mark_defense = models.CharField(max_length=255)
    mark_basic = models.CharField(max_length=255)
    rank = models.CharField(max_length=255)


# class City(BaseModel):
#    pass
#
#
# class School(BaseModel):
#    pass
#
#
# class Department(BaseModel):
#    pass
#
#
# class Section(BaseModel):
#    pass
#
#
# class Basic(BaseModel):
#    pass


def get_user_by_id(user_id):
    try:
        return User.objects.get(pk=user_id)
    except User.DoesNotExist:
        return None
