# -*- coding: utf-8 -*-
from django import forms
from app.models import UserProfile
from django.utils.translation import ugettext_lazy as _
from .models import Category
from .models import Ticket
from .fields import UniqueCodeField


class TicketForm(forms.ModelForm):
    title = forms.CharField(
        label=_(u"Название"),
        initial=_(u"Новая заявка")
    )
    category = forms.ModelChoiceField(
        label=_(u"Категория"),
        queryset=Category.objects.all(),
        required=False,
    )
    image = forms.FileField(
        label=_(u"Изображние"),
        required=False
    )
    code = UniqueCodeField()

    class Meta:
        model = Ticket
        fields = ("title", "unique_code", "category",
                  "department", "section", "basic")

    def __init__(self, *args, **kwargs):
        super(TicketForm, self).__init__(*args, **kwargs)

        if self.instance:
            try:
                self.fields["image"].image_url = \
                    self.instance.image.url
            except ValueError:
                pass


class SignInForm(forms.ModelForm):
    email = forms.EmailField(label="Email")
    password = forms.CharField(label=_(u"Пароль"),
                               widget=forms.PasswordInput)

    class Meta:
        model = UserProfile
        fields = ('email', 'password')


class SignUpForm(forms.Form):

    """ Require email address when a user signs up """
    email = forms.EmailField(label=_(u"Email адрес"), max_length=75, )
    password = forms.CharField(label=_(u"Пароль"), max_length=128,
                               widget=forms.PasswordInput)
    name = forms.CharField(label=_(u"Ваше имя"), max_length=300,
                           required=False, initial=_(u'Администратор'))
    phone = forms.CharField(label=_(u"Номер телефона"),
                            max_length=13, required=False)
    org_name = forms.CharField(label=_(u"Название организации"),
                               max_length=100, required=True,
                               widget=forms.TextInput(attrs={
                                   'placeholder': 'OOO Romashka'}))

    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            UserProfile.objects.get(email=email)
            raise forms.ValidationError("This email address already exists. "
                                        "Did you forget your password?")
        except UserProfile.DoesNotExist:
            return email
