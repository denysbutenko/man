"""
Base Form field output tag
"""
from django import template

register = template.Library()


@register.inclusion_tag('tags/base_fields.html')
def base_ff(*fields):
    if len(fields) == 1 and isinstance(fields[0], basestring):
        return {
            'field': fields[0],
        }
    else:
        return {
            'fields': fields,
        }
