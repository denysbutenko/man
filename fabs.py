import sys, os
sys.path.insert(0, os.path.join(os.path.abspath(os.path.dirname(__file__)), '..', '..'))
from fabfile import *

def my_deploy():
    env.domain = 'man'
    env.repository = 'git@bitbucket.org:denysbutenko/man.git'

    deploy()

if __name__ == '__main__':
    # hack for pycharm run configuration.
    import subprocess, sys
    subprocess.call(['fab', '-f', __file__] + sys.argv[1:])
